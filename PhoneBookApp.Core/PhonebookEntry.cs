﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhoneBookApp.Core
{
    public class PhonebookEntry
    {
        [BsonId]
        [BsonIgnoreIfNull]
        [JsonIgnore]
        public BsonObjectId _id { get; set; }

        public long PhonebookID { get; set; }

        public long PhonebookEntryID { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }
    }
}
