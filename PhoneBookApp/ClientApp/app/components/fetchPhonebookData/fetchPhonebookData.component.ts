import { Component, Inject } from '@angular/core';
import { Http, HttpModule, Response } from '@angular/http';
import { getBaseUrl } from '../../app.browser.module';
import { RequestOptions } from '@angular/http/src/base_request_options';

@Component({
    selector: 'fetchPhoneBookData',
    templateUrl: './fetchPhoneBookData.component.html'
})
export class FetchPhonebookDataComponent {
    public phonebooks: Phonebook[];
    public phonebookEntries: PhonebookEntry[];

    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/phonebook/GetPhonebooks').subscribe(res => {
            this.phonebooks = res.json() as Phonebook[];

        }, error => console.error(error));
    }

    public async getPhonebookEntries(phonebookID: number): Promise<IPhonebookEntry[]> {

		await this.http.get(getBaseUrl() + 'api/phonebook/GetPhonebookEntries/' + phonebookID).subscribe(result => {
            this.phonebookEntries = result.json() as PhonebookEntry[];
        }, error => console.error(error));

        return this.phonebookEntries;
    }

    public async searchNumber(name: string): Promise<IPhonebookEntry[]> {
        let numbers: PhonebookEntry[] = [];

        await this.http.get(getBaseUrl() + 'api/phonebook/GetPhonebookEntriesMatchingName/' + name).subscribe(result => {
            numbers = result.json() as PhonebookEntry[];
        }, error => console.error(error));

        return numbers;
    }

    public async addPhonebookEntry(entry: IPhonebookEntry): Promise<void> {
        await this.http.post(getBaseUrl() + 'api/phonebook/AddPhonebookEntry/', JSON.stringify(entry)).subscribe(result => { }, error => console.error(error));
    }
}

export interface IPhonebookEntry {
    phonebookID: number;
    phonebookEntryID: number;
    name: string;
    phoneNumber: string;
}

export class Phonebook implements IPhonebook {
    constructor(public phonebookID: number, public phonebookName: string, public entries: IPhonebookEntry[]) {}
}

export class PhonebookEntry implements IPhonebookEntry {
    constructor(public phonebookID: number, public phonebookEntryID: number, public name: string, public phoneNumber: string) { }
}

export interface IPhonebook {
    phonebookID: number;
    phonebookName: string;
    entries: PhonebookEntry[];
}
